#!/usr/bin/env python
# (c) 2017 Felipe Sanches <juca@members.fsf.org>
# Licensed under GPLv3 (or later)

import sys
import urllib
import re

lines = open("example/wiki.txt").readlines()
out = open("output.txt", "w")

driver_data = {}

def load_and_parse_lkddb(first_letter):
  r = urllib.urlopen("https://cateee.net/lkddb/web-lkddb/index_{}.html".format(first_letter))
  data = r.read()
  data = data.split("CONFIG_")
  data.pop(0)
  results = {}
  for item in data:
    try:
      name, rest = item.split("</a> (<small>")
      version = rest.split("&")[0]
      version = version.split(";")[0]
      version = version.split(",")[0]
      results[name] = version
    except:
      pass

  driver_data[first_letter] = results

kernel_dates = {}
def get_dates():
  data = urllib.urlopen("https://kernelnewbies.org/LinuxVersions").read()
  items = data.split("\">Linux ")
  items.pop(0)

  MONTHS = {
    "January,": "01",
    "February,": "02",
    "March,": "03",
    "April,": "04",
    "May,": "05",
    "June,": "06",
    "July,": "07",
    "August,": "08",
    "September,": "09",
    "October,": "10",
    "November,": "11",
    "December,": "12",
    "December": "12" #That's a typo in the webpage
  }
  for item in items:
    try:
      version, date = item.split("</a> Released ")
      date = date.split(" (")[0]
      date = date.split(" 70")[0] # thats a typo in the webpage!
      date = date.split(" <a ")[0]
      print (date)
      day, month, year = date.split(" ")
      if int(day) < 10:
       day = "0{}".format(day)
      month = MONTHS[month]
      date = " {}-{}-{} ".format(year, month, day)
      kernel_dates[version] = date
    except:
      pass

def get_data(driver_name):
  if driver_name[0] not in driver_data:
    load_and_parse_lkddb(driver_name[0])

  try:
    return driver_data[driver_name[0]][driver_name]
  except:
    return None

get_dates()

for line in lines:
  if "||" not in line or line[0] != "|":
    out.write(line)
    continue

  items = line.split("||")

  # Parse this:
  # | [[LinuxLibre:GREYBUS_FIRMWARE|GREYBUS_FIRMWARE]] 
  driver_name = items[0].split("]]")[0].split("|")[-1]

  kernel_version = get_data(driver_name)
  print "{}: {}".format(driver_name, kernel_version)
  if kernel_version is not None:
    items[2] = " {} ".format(kernel_version)
    if kernel_version in kernel_dates:
      items[3] = kernel_dates[kernel_version]

  out.write("||".join(items))

out.close()
